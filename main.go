package main

import (
	"flag"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"net/url"
	"os"
	"os/signal"
	"time"
)

var host = flag.String("host", "127.0.0.1:6700", "http service address")
var token = flag.String("token", "", "access_token")
var 客户端 *websocket.Conn

func main() {
	//log.Fatal("")
	flag.Parse()
	log.SetFlags(0)

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	u := url.URL{Scheme: "ws", Host: *host, Path: "/api/", RawQuery: "access_token=" + *token}
	log.Printf("连接到服务器 %s", u.String())

	var err error
	客户端, _, err = websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer 客户端.Close()

	done := make(chan struct{})

	go func() {
		defer close(done)
		for {
			_, message, err := 客户端.ReadMessage()
			if err != nil {
				log.Println("read:", err)
				return
			}
			log.Printf("recv: %s", message)
			JSOnevent(string(message))
		}
	}()

	JSRun()

	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()

	for {
		select {
		case <-done:
			return
		//case t := <-ticker.C:
		//	err := c.WriteMessage(websocket.TextMessage, []byte(t.String()))
		//	if err != nil {
		//		log.Println("write:", err)
		//		return
		//	}
		case <-interrupt:
			log.Println("interrupt")

			// Cleanly close the connection by sending a close message and then
			// waiting (with timeout) for the server to close the connection.
			err := 客户端.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				log.Println("退出 write close:", err)
				return
			}
			select {
			case <-done:
			case <-time.After(time.Second):
			}
			return
		}
	}

	log.Printf("已退出 %s", u.String())

}

func 发群消息(群号 int64, 消息 string) {
	内容 := fmt.Sprintf(`{
    "action": "send_group_msg",
    "params": {
        "group_id": %d,
        "message": "%s"
    }
}`, 群号, 消息)
	log.Printf("发送 %s", 内容)

	err := 客户端.WriteMessage(websocket.TextMessage, []byte(内容))
	if err != nil {
		log.Println("write:", err)
		return
	}
}
func 撤回消息(消息 int32) {
	内容 := fmt.Sprintf(`{
    "action": "delete_msg",
    "params": {
        "message_id": %d,
    }
}`, 消息)
	log.Printf("发送 撤回 %s", 内容)

	err := 客户端.WriteMessage(websocket.TextMessage, []byte(内容))
	if err != nil {
		log.Println("write:", err)
		return
	}
}
func 发送(内容 string) {
	//	内容:=fmt.Sprintf(`{
	//    "action": "send_group_msg",
	//    "params": {
	//        "group_id": %d,
	//        "message": "%s"
	//    }
	//}`,群号,消息)
	log.Printf("发送 %s", 内容)

	err := 客户端.WriteMessage(websocket.TextMessage, []byte(内容))
	if err != nil {
		log.Println("write:", err)
		return
	}
}

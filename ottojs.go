package main

import (
	"errors"
	"github.com/fsnotify/fsnotify"
	"github.com/robertkrimen/otto"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"
)

var vms = make(map[string]*otto.Otto)
var 全局变量 = make(map[string]string)

func NewOttoVm() *otto.Otto {
	vm := otto.New()
	vm.Run(`
function 键值组() {
	this.arrmaps = new Array();
	this.大小 = function () {
		return this.arrmaps.length;
	}
	this.为空 = function () {
		return (this.arrmaps.length < 1);
	}
	this.清空 = function () {
		this.arrmaps = new Array();
	}
	this.添加 = function (_key, _value) {
		this.arrmaps.push({
			key: _key,
			value: _value
		});
	}
	this.取值 = function (_key) {
		try {
			for (i = 0; i < this.arrmaps.length; i++) {
				if (this.arrmaps[i].key == _key) {
					return this.arrmaps[i].value;
				}
			}
		} catch (e) {
			return null;
		}
	}
	this.删除 = function (_key) {
		var flag = false;
		try {
			for (i = 0; i < this.arrmaps.length; i++) {
				if (this.arrmaps[i].key == _key) {
					this.arrmaps.splice(i, 1);
					return true;
				}
			}
		} catch (e) {
			flag = false;
		}
		return flag;
	}
	this.取键 = function (_value) {
		try {
			for (i = 0; i < this.arrmaps.length; i++) {
				if (this.arrmaps[i].value == _value) {
					return this.arrmaps[i].key;
				}
			}
		} catch (e) {
			return null;
		}
	}
	this.索引 = function (_index) {
		if (_index < 0 || _index >= this.arrmaps.length) {
			return null;
		}
		return this.arrmaps[_index];
	}
	this.包含键 = function (_key) {
		var flag = false;
		try {
			for (i = 0; i < this.arrmaps.length; i++) {
				if (this.arrmaps[i].key == _key) {
					flag = true;
				}
			}
		} catch (e) {
			flag = false;
		}
		return flag;
	}
	this.包含值 = function (_value) {
		var flag = false;
		try {
			for (i = 0; i < this.arrmaps.length; i++) {
				if (this.arrmaps[i].value == _value) {
					flag = true;
				}
			}
		} catch (e) {
			flag = false;
		}
		return flag;
	}
	this.值组 = function () {
		var arr = new Array();
		for (i = 0; i < this.arrmaps.length; i++) {
			arr.push(this.arrmaps[i].value);
		}
		return arr;
	}
	this.键组 = function () {
		var arr = new Array();
		for (i = 0; i < this.arrmaps.length; i++) {
			arr.push(this.arrmaps[i].key);
		}
		return arr;
	}
}
`)
	_, err := vm.Run(`

var faceid = [14,1,2,3,4,5,6,7,8,9,10,11,12,13,0,15,16,96,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,172,182,179,173,174,212,175,178,177,180,181,176,183,112,89,113,114,115,171,60,61,46,63,64,116,66,67,53,54,55,56,57,117,59,75,74,69,49,76,77,78,79,118,119,120,121,122,123,124,42,85,43,41,86,125,126,127,128,129,130,131,132,133,134,136,137,138,140,144,145,146,147,148,151,158,168,169,188,192,184,185,190,187,193,194,197,198,199,200,201,202,203,204,205,206,207,208,210,211,247,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,276,277,278,279,280,281,282,283,284,285,286,287,288,289,]
var 表情名 = ["微笑","撇嘴","色","发呆","得意","流泪","害羞","闭嘴","睡","大哭","尴尬","发怒","调皮","呲牙","惊讶","难过","酷","冷汗","抓狂","吐","偷笑","可爱","白眼","傲慢","饥饿","困","惊恐","流汗","憨笑","大兵","奋斗","咒骂","疑问","嘘","晕","折磨","衰","骷髅","敲打","再见","擦汗","抠鼻","鼓掌","糗大了","坏笑","左哼哼","右哼哼","哈欠","鄙视","委屈","快哭了","阴险","亲亲","吓","可怜","眨眼睛","笑哭","doge","泪奔","无奈","托腮","卖萌","斜眼笑","喷血","惊喜","骚扰","小纠结","我最美","菜刀","西瓜","啤酒","篮球","乒乓","茶","咖啡","饭","猪头","玫瑰","凋谢","示爱","爱心","心碎","蛋糕","闪电","炸弹","刀","足球","瓢虫","便便","月亮","太阳","礼物","拥抱","强","弱","握手","胜利","抱拳","勾引","拳头","差劲","爱你","NO","OK","爱情","飞吻","跳跳","发抖","怄火","转圈","磕头","回头","跳绳","挥手","激动","街舞","献吻","左太极","右太极","双喜","鞭炮","灯笼","K歌","喝彩","祈祷","爆筋","棒棒糖","喝奶","飞机","钞票","药","手枪","蛋","红包","河蟹","羊驼","菊花","幽灵","大笑","不开心","冷漠","呃","好棒","拜托","点赞","无聊","托脸","吃","送花","害怕","花痴","小样儿","飙泪","我不看","口罩护体", "搬砖中", "忙到飞起", "脑阔疼", "沧桑", "捂脸", "辣眼睛", "哦呦", "头秃", "问号脸", "暗中观察", "emm", "吃瓜", "呵呵哒", "我酸了", "太南了", "辣椒酱", "汪汪", "汗", "打脸", "击掌", "无眼笑", "敬礼", "狂笑", "面无表情", "摸鱼", "魔鬼笑", "哦", "请", "睁眼",]
String.prototype.startWith = function (s) { if (s == null||s==""||this.length==0||s.length> this.length) return false; if (this.substr(0,s.length) == s)return true; else return false; return true;}
String.prototype.endWith = function(s) { if (s == null || s == "" || this.length == 0 || s.length > this.length) return false; if (this.substring(this.length - s.length) == s)  return true;  else  return false; }
String.prototype.结尾 = function(s) { if (s == null || s == "" || this.length == 0 || s.length > this.length) return false; if (this.substring(this.length - s.length) == s)  return true;  else  return false; }
String.prototype.开头 = function (s) { if (s == null||s==""||this.length==0||s.length> this.length) return false; if (this.indexOf(s) == 0)return true; else return false; return true;}
String.prototype.表情转码 = function(){return this.replace(/\[\/(.*?)\]/ig,function(all,t){var i = 表情名.indexOf(t);if(i!=-1)return "[CQ:face,id="+faceid[i]+"]";return all;})}
String.prototype.表情解码 = function(){return this.replace(/\[CQ:face,id=(\d+)\]/ig,function(all,t){var i = faceid.indexOf(Number(t)); if(i!=-1)return "[/"+表情名[i]+"]";return all;})}
String.prototype.包含 = function (s) {return this.indexOf(s)!=-1}
function 随机取(arr) { return arr[Math.floor(Math.random() * arr.length)]; }
function 随机数(max) { return Math.floor(Math.random() * max); }
function 概率(p) { return Math.random() < (p / 100); }


`)

	_ = vm.Set("netget", func(call otto.FunctionCall) otto.Value {
		url, _ := call.Argument(0).ToString()
		result, _ := vm.ToValue(Get(url))
		return result
	})
	_ = vm.Set("netpost", func(call otto.FunctionCall) otto.Value {
		url, _ := call.Argument(0).ToString()
		body, _ := call.Argument(1).ToString()
		result, _ := vm.ToValue(Post(url, body))
		return result
	})
	_ = vm.Set("发群消息", func(call otto.FunctionCall) otto.Value {
		//fmt.Printf("Hello, %s.\n", call.Argument(0).String())
		群号, _ := call.Argument(0).ToInteger()
		消息, _ := call.Argument(1).ToString()
		if len(消息) > 1500 {
			limited := []rune(消息)
			limited = limited[:100]
			消息 = string(limited) + "..."
		}
		发群消息(群号, 消息)
		//WebServer.Bot.CQSendGroupMessage(群号, 消息, false)
		return otto.Value{}
	})
	_ = vm.Set("发送", func(call otto.FunctionCall) otto.Value {
		//fmt.Printf("Hello, %s.\n", call.Argument(0).String())
		//群号, _ := call.Argument(0).ToInteger()
		消息, _ := call.Argument(0).ToString()
		//if len(消息)>1500 {
		//	limited := []rune(消息)
		//	limited = limited[:100]
		//	消息=string(limited)+"..."
		//}
		发送(消息)
		//WebServer.Bot.CQSendGroupMessage(群号, 消息, false)
		return otto.Value{}
	})
	//_ = vm.Set("发消息", func(call otto.FunctionCall) otto.Value {
	//	//号, _ := call.Argument(0).ToInteger()
	//	//消息, _ := call.Argument(1).ToString()
	//	//WebServer.Bot.CQSendPrivateMessage(号, 消息, false)
	//	return otto.Value{}
	//})
	_ = vm.Set("撤回消息", func(call otto.FunctionCall) otto.Value {
		号, _ := call.Argument(0).ToInteger()
		//println(号,err)
		//println(int32(号))
		//WebServer.Bot.CQDeleteMessage(int32(号))
		撤回消息(int32(号))
		return otto.Value{}
	})
	//_ = vm.Set("处理加好友请求", func(call otto.FunctionCall) otto.Value {
	//	//p1, _ := call.Argument(0).ToString()
	//	//p2, _ := call.Argument(1).ToBoolean()
	//	//WebServer.Bot.CQProcessFriendRequest(p1, p2)
	//	return otto.Value{}
	//})
	//_ = vm.Set("处理加群请求", func(call otto.FunctionCall) otto.Value {
	//	//p1, _ := call.Argument(0).ToString()
	//	//p2, _ := call.Argument(1).ToString()
	//	//p3, _ := call.Argument(1).ToString()
	//	//p4, _ := call.Argument(1).ToBoolean()
	//	//WebServer.Bot.CQProcessGroupRequest(p1, p2, p3, p4)
	//	return otto.Value{}
	//})
	_ = vm.Set("读全局变量", func(call otto.FunctionCall) otto.Value {
		p1, _ := call.Argument(0).ToString()
		//p2, _ := call.Argument(1).ToString()
		res := 全局变量[p1]
		result, _ := vm.ToValue(res)
		return result
	})
	_ = vm.Set("写全局变量", func(call otto.FunctionCall) otto.Value {
		p1, _ := call.Argument(0).ToString()
		p2, _ := call.Argument(1).ToString()
		//p3, _ := call.Argument(1).ToString()
		//p4, _ := call.Argument(1).ToBoolean()
		全局变量[p1] = p2
		return otto.Value{}
	})
	_ = vm.Set("执行", func(call otto.FunctionCall) otto.Value {
		p1, _ := call.Argument(0).ToString()
		//println(p1)
		//p2, _ := call.Argument(1).ToString()
		//p3, _ := call.Argument(1).ToString()
		//p4, _ := call.Argument(1).ToBoolean()
		res, s := runUnsafe(p1)
		//log.Println("s",res,s)
		if s {
			result, _ := vm.ToValue(res)
			return result
		} else {
			result, _ := vm.ToValue(res + "null")
			return result
		}

	})
	if err != nil {
		log.Infof("脚本文件错误:" + err.Error())
	}
	//vm.SetDebuggerHandler()

	return vm
}
func JSOnevent(m string) {
	for f, z := range vms {
		_, err := z.Run("onevent(" + m + ")")
		if err != nil {
			log.Infof(f + ": \n" + err.Error())
		}
	}

}

func JSRun() {
	path, _ := os.Getwd()
	//println(path+ string(os.PathSeparator) +"js"+ string(os.PathSeparator))
	path = path + string(os.PathSeparator) + "js"
	files, _ := GetFiles(path)
	for _, z := range files {
		txt, err := ReadAll(z)
		if err != nil {
			log.Infof(err.Error())
		} else {
			vm := NewOttoVm()
			_, err = vm.Run(string(txt))
			if err != nil {
				//log.Infof("检查更新完成. 当前已运行最新版本.")
				log.Infof("脚本文件初始化错误 未加载 :\n" + z + "\n" + err.Error())
			} else {
				log.Infof("已加载脚本 " + z)
				vms[z] = vm
				//vms = append(vms,vm)
			}

		}
		//println(z)

	}

	go 脚本监控(path)
}

func 脚本监控(path string) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()
	done := make(chan bool)
	go func() {
		for {
			select {
			case ev := <-watcher.Events:
				{
					//log.Println(ev)
					//判断事件发生的类型，如下5种
					// Create 创建
					// Write 写入
					// Remove 删除
					// Rename 重命名
					// Chmod 修改权限
					ok := strings.HasSuffix(ev.Name, ".js")
					if ok {
						if ev.Op&fsnotify.Remove == fsnotify.Remove || ev.Op&fsnotify.Rename == fsnotify.Rename {
							delete(vms, ev.Name)
						}
						if ev.Op&fsnotify.Write == fsnotify.Write || ev.Op&fsnotify.Create == fsnotify.Create {
							z := ev.Name
							txt, err := ReadAll(z)
							if err != nil {
								log.Infof(err.Error())
							} else {
								vm := NewOttoVm()
								_, err = vm.Run(string(txt))
								if err != nil {
									log.Infof("脚本文件初始化错误 未加载 :\n" + z + "\n" + err.Error())
								} else {
									log.Infof("已重新加载脚本 " + z)
									delete(vms, z)
									vms[z] = vm
									//vms = append(vms,vm)
									//for k,_ := range vms{
									//	log.Println("k-"+k)
									//}
								}

							}
							//log.Println("写入文件 : ", ev.Name);
						}
					}

					//if ev.Op&fsnotify.Create == fsnotify.Create {
					//	log.Println("创建文件 : ", ev.Name);
					//}
					//if ev.Op&fsnotify.Write == fsnotify.Write {
					//	log.Println("写入文件 : ", ev.Name);
					//}
					//if ev.Op&fsnotify.Remove == fsnotify.Remove {
					//	log.Println("删除文件 : ", ev.Name);
					//}
					//if ev.Op&fsnotify.Rename == fsnotify.Rename {
					//	log.Println("重命名文件 : ", ev.Name);
					//}
					//if ev.Op&fsnotify.Chmod == fsnotify.Chmod {
					//	log.Println("修改权限 : ", ev.Name);
					//}
				}
			case err := <-watcher.Errors:
				{
					log.Println("error : ", err)
					return
				}
			}
		}
	}()

	err = watcher.Add(path)
	if err != nil {
		log.Fatal(err)
	}
	<-done
}
func ReadAll(filePth string) ([]byte, error) {
	f, err := os.Open(filePth)
	if err != nil {
		return nil, err
	}

	return ioutil.ReadAll(f)
}

//获取指定目录下的所有文件和目录
func GetFilesAndDirs(dirPth string) (files []string, dirs []string, err error) {
	println(dirPth)
	dir, err := ioutil.ReadDir(dirPth)
	if err != nil {
		return nil, nil, err
	}

	PthSep := string(os.PathSeparator)
	//suffix = strings.ToUpper(suffix) //忽略后缀匹配的大小写

	for _, fi := range dir {
		if fi.IsDir() { // 目录, 递归遍历
			dirs = append(dirs, dirPth+PthSep+fi.Name())
			GetFilesAndDirs(dirPth + PthSep + fi.Name())
		} else {
			// 过滤指定格式
			ok := strings.HasSuffix(fi.Name(), ".go")
			if ok {
				files = append(files, dirPth+PthSep+fi.Name())
			}
		}
	}

	return files, dirs, nil
}

func GetFiles(dirPth string) (files []string, err error) {
	//println(dirPth)
	dir, err := ioutil.ReadDir(dirPth)
	if err != nil {
		return nil, err
	}

	PthSep := string(os.PathSeparator)
	//suffix = strings.ToUpper(suffix) //忽略后缀匹配的大小写

	for _, fi := range dir {
		if fi.IsDir() { // 目录, 递归遍历
			//dirs = append(dirs, dirPth+PthSep+fi.Name())
			//GetFilesAndDirs(dirPth + PthSep + fi.Name())
		} else {
			// 过滤指定格式
			ok := strings.HasSuffix(fi.Name(), ".js")
			if ok {
				files = append(files, dirPth+PthSep+fi.Name())
			}
		}
	}

	return files, nil
}

//获取指定目录下的所有文件,包含子目录下的文件
func GetAllFiles(dirPth string) (files []string, err error) {
	var dirs []string
	dir, err := ioutil.ReadDir(dirPth)
	if err != nil {
		return nil, err
	}

	PthSep := string(os.PathSeparator)
	//suffix = strings.ToUpper(suffix) //忽略后缀匹配的大小写

	for _, fi := range dir {
		if fi.IsDir() { // 目录, 递归遍历
			dirs = append(dirs, dirPth+PthSep+fi.Name())
			GetAllFiles(dirPth + PthSep + fi.Name())
		} else {
			// 过滤指定格式
			ok := strings.HasSuffix(fi.Name(), ".go")
			if ok {
				files = append(files, dirPth+PthSep+fi.Name())
			}
		}
	}

	// 读取子目录下文件
	for _, table := range dirs {
		temp, _ := GetAllFiles(table)
		for _, temp1 := range temp {
			files = append(files, temp1)
		}
	}

	return files, nil
}
func runUnsafe(unsafe string) (r string, s bool) {
	//start := time.Now()
	defer func() {
		//duration := time.Since(start)
		if caught := recover(); caught != nil {
			if caught == halt {
				//fmt.Fprintf(os.Stderr, "Some code took to long! Stopping after: %v\n", duration)
				s = false
				return
			}
			panic(caught) // Something else happened, repanic!

		}
		//fmt.Fprintf(os.Stderr, "Ran code successfully: %v\n", duration)
		s = true
	}()

	vm := NewOttoVm()
	_ = vm.Set("执行", func(call otto.FunctionCall) otto.Value {
		//p1, _ := call.Argument(0).ToString()
		//p2, _ := call.Argument(1).ToString()
		//p3, _ := call.Argument(1).ToString()
		//p4, _ := call.Argument(1).ToBoolean()

		result, _ := vm.ToValue("套娃失败")
		return result

	})
	vm.Interrupt = make(chan func(), 1) // The buffer prevents blocking

	go func() {
		time.Sleep(5 * time.Second) // Stop after two seconds
		vm.Interrupt <- func() {
			panic(halt)

		}
	}()

	ret, err := vm.Run(unsafe) // Here be dragons (risky code)
	//log.Println(ret,err,s)
	if err != nil {
		return err.Error(), false
	}
	rett, err := ret.ToString()
	//log.Println(rett,err,s)

	if err != nil {
		return err.Error(), false
	}
	return rett, true

}

var halt = errors.New("Stahp")

func Get(url string) string {
	if !strings.HasPrefix(url, "http") {
		url = "http://" + url
	}
	res, err := http.Get(url)
	if err != nil {
		log.Infof(err.Error())
		return "访问错误"
	}
	//println(res.ContentLength)
	if res.ContentLength < 1024*1024 {
		robots, err := ioutil.ReadAll(res.Body)
		res.Body.Close()

		if err != nil {
			log.Infof(err.Error())

			return "读取错误"
		}
		return string(robots)

	} else {
		res.Body.Close()
		return "信息量太大 太长没看~"
	}

}

func Post(url string, body string) string {
	if !strings.HasPrefix(url, "http") {
		url = "http://" + url
	}
	res, err := http.Post(url, "application/x-www-form-urlencoded", strings.NewReader(body))
	if err != nil {
		log.Infof(err.Error())
		return "访问错误"
	}
	//println(res.ContentLength)
	if res.ContentLength < 1024*1024 {
		robots, err := ioutil.ReadAll(res.Body)
		res.Body.Close()

		if err != nil {
			log.Infof(err.Error())

			return "读取错误"
		}
		return string(robots)

	} else {
		res.Body.Close()
		return "信息量太大 太长没看~"
	}

}

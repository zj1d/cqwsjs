package main

import (
	"github.com/fsnotify/fsnotify"
	"github.com/robertkrimen/otto"
	"log"
	"os"
	"strings"
	"testing"
)

func TestPath(t *testing.T) {
	println(t.TempDir())
	path, _ := os.Getwd()
	println(path + string(os.PathSeparator) + "js" + string(os.PathSeparator))
	files, _ := GetFiles("F:\\go\\go-cqhttp-0.9.34\\js")
	for _, z := range files {
		println(z)

		vm := otto.New()
		_, _ = vm.Run(`
    abc = 2 + 2;
    console.log("The value of abc is " + abc); // 4
`)

	}

	t.Log(runUnsafe(`
//while(true){}
for(;;){
//1
//console.log(1)
}
`))

}
func TestNet(t *testing.T) {
	vm := otto.New()
	vm.Set("twoPlus", func(call otto.FunctionCall) otto.Value {
		right, _ := call.Argument(0).ToInteger()
		result, _ := vm.ToValue(2 + right)
		return result
	})
	vm.Set("netget", func(call otto.FunctionCall) otto.Value {
		url, _ := call.Argument(0).ToString()
		result, _ := vm.ToValue(Get(url))
		//Get(url)
		return result
	})
	vm.Set("netpost", func(call otto.FunctionCall) otto.Value {
		url, _ := call.Argument(0).ToString()
		body, _ := call.Argument(1).ToString()
		result, _ := vm.ToValue(Post(url, body))
		//Get(url)
		return result
	})
	_, err := vm.Run(`
console.log(netpost("http://www.01happy.com/demo/accept.php","a=啊哦"))
console.log(netget("https://stream7.iqilu.com/10339/upload_transcode/202002/18/20200218114723HDu3hhxqIT.mp4"))
var ret = netget("http://i.itpk.cn/api.php?question=笑话").replace(/\\r\\n/g,"")
ret = unescape(ret.replace(/\(u[0-9a-fA-F]{4}\)/gm, '%$1'));
console.log(ret)
console.log(JSON.parse(ret).content)
console.log(JSON.parse("'"+ret+"'").content)
`)
	if err != nil {
		println(err.Error())

	}
	//"{\"title\":\"\\u627e\\u627e\\u6709\\u6ca1\\u6709\\u5403\\u5c4e\\u7684\",\"content\":\"\\u6211\\u5988\\u6211\\u59d0\\u770b\\u7535\\u89c6\\uff0c\\r\\n\\u59d0\\uff1a\\u5988\\uff0c\\u4e0a\\u9762\\u4ed6\\u4eec\\u5403\\u571f\\u8c46\\u70e7\\u8089\\u4e86\\uff0c\\u6211\\u4e5f\\u60f3\\u5403\\u3002\\r\\n\\u5988\\uff1a\\u54e6\\r\\n\\u59d0\\uff1a\\u5988\\uff0c\\u4f60\\u770b\\u4eba\\u5bb6\\u5403\\u9c7c\\u9999\\u8089\\u4e1d\\u4e86\\uff0c\\u6211\\u4e5f\\u60f3\\u5403\\u3002\\r\\n\\u5988\\uff1a\\u6362\\u53f0\\uff0c\\u627e\\u627e\\u6709\\u6ca1\\u6709\\u5403\\u5c4e\\u7684\\r\\n\\u7136\\u540e\\u5c31\\u6ca1\\u6709\\u7136\\u540e\\u4e86\\u2026\\u2026\\r\\n\"}"
}
func TestPPet(t *testing.T) {
	vm := otto.New()
	_, err := vm.Run(`
var faceid = [14,1,2,3,4,5,6,7,8,9,10,11,12,13,0,15,16,96,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,172,182,179,173,174,212,175,178,177,180,181,176,183,112,89,113,114,115,171,60,61,46,63,64,116,66,67,53,54,55,56,57,117,59,75,74,69,49,76,77,78,79,118,119,120,121,122,123,124,42,85,43,41,86,125,126,127,128,129,130,131,132,133,134,136,137,138,140,144,145,146,147,148,151,158,168,169,188,192,184,185,190,187,193,194,197,198,199,200,201,202,203,204,205,206,207,208,210,211,247,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,276,277,278,279,280,281,282,283,284,285,286,287,288,289,]
var facename = ["撇嘴","色","发呆","得意","流泪","害羞","闭嘴","睡","大哭","尴尬","发怒","调皮","呲牙","惊讶","难过","酷","冷汗","抓狂","吐","偷笑","可爱","白眼","傲慢","饥饿","困","惊恐","流汗","憨笑","大兵","奋斗","咒骂","疑问","嘘","晕","折磨","衰","骷髅","敲打","再见","擦汗","抠鼻","鼓掌","糗大了","坏笑","左哼哼","右哼哼","哈欠","鄙视","委屈","快哭了","阴险","亲亲","吓","可怜","眨眼睛","笑哭","doge","泪奔","无奈","托腮","卖萌","斜眼笑","喷血","惊喜","骚扰","小纠结","我最美","菜刀","西瓜","啤酒","篮球","乒乓","茶","咖啡","饭","猪头","玫瑰","凋谢","示爱","爱心","心碎","蛋糕","闪电","炸弹","刀","足球","瓢虫","便便","月亮","太阳","礼物","拥抱","强","弱","握手","胜利","抱拳","勾引","拳头","差劲","爱你","NO","OK","爱情","飞吻","跳跳","发抖","怄火","转圈","磕头","回头","跳绳","挥手","激动","街舞","献吻","左太极","右太极","双喜","鞭炮","灯笼","K歌","喝彩","祈祷","爆筋","棒棒糖","喝奶","飞机","钞票","药","手枪","蛋","红包","河蟹","羊驼","菊花","幽灵","大笑","不开心","冷漠","呃","好棒","拜托","点赞","无聊","托脸","吃","送花","害怕","花痴","小样儿","飙泪","我不看","口罩护体", "搬砖中", "忙到飞起", "脑阔疼", "沧桑", "捂脸", "辣眼睛", "哦呦", "头秃", "问号脸", "暗中观察", "emm", "吃瓜", "呵呵哒", "我酸了", "太南了", "辣椒酱", "汪汪", "汗", "打脸", "击掌", "无眼笑", "敬礼", "狂笑", "面无表情", "摸鱼", "魔鬼笑", "哦", "请", "睁眼",]
String.prototype.startWith = function (s) { if (s == null||s==""||this.length==0||s.length> this.length) return false; if (this.substr(0,s.length) == s)return true; else return false; return true;}
String.prototype.endWith = function(s) { if (s == null || s == "" || this.length == 0 || s.length > this.length) return false; if (this.substring(this.length - s.length) == s)  return true;  else  return false; }
String.prototype.结尾 = function(s) { if (s == null || s == "" || this.length == 0 || s.length > this.length) return false; if (this.substring(this.length - s.length) == s)  return true;  else  return false; }
String.prototype.开头 = function (s) { if (s == null||s==""||this.length==0||s.length> this.length) return false; if (this.substr(0,s.length) == s)return true; else return false; return true;}
String.prototype.表情转码 = function(){return this.replace(/\[\/(.*?)\]/ig,function(all,t){var i = facename.indexOf(t);if(i!=-1)return "[CQ:face,id="+faceid[i]+"]";return all;})}
String.prototype.包含 = function (s) {return this.indexOf(s)!=-1}
console.log("123".startWith("1"))
console.log("123".开头("1"))
console.log("123".开头("2"))
console.log("123".结尾("3"))




console.log(facename.indexOf("撇嘴"))
console.log("123[/doge][/dge][/白眼]".replace(/\[\/(.*?)\]/ig,function(all,t){
//console.log(all)
//console.log(t)
var i = facename.indexOf(t)
if(i!=-1)
return "[CQ:face,id="+faceid[i]+"]"
return all;
}))
console.log("123/doge [/doge] [ /doge] [/dog]e".表情转码())
`)
	if err != nil {
		println(err.Error())

	}

}
func TestIF(t *testing.T) {
	vm := otto.New()
	vm.Run(`
if(true)
console.log(1)
如果(真)
console.log(2)
a = [0,1,2]
a[3]=3
console.log("213".长度)
console.log(a.长度)
`)
}

func TestFilechange(t *testing.T) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()
	done := make(chan bool)
	go func() {
		for {
			select {
			case ev := <-watcher.Events:
				{
					//判断事件发生的类型，如下5种
					// Create 创建
					// Write 写入
					// Remove 删除
					// Rename 重命名
					// Chmod 修改权限
					if ev.Op&fsnotify.Create == fsnotify.Create {
						log.Println("创建文件 : ", ev.Name)
					}
					if ev.Op&fsnotify.Write == fsnotify.Write {
						log.Println("写入文件 : ", ev.Name)
					}
					if ev.Op&fsnotify.Remove == fsnotify.Remove {
						log.Println("删除文件 : ", ev.Name)
					}
					if ev.Op&fsnotify.Rename == fsnotify.Rename {
						log.Println("重命名文件 : ", ev.Name)
					}
					if ev.Op&fsnotify.Chmod == fsnotify.Chmod {
						log.Println("修改权限 : ", ev.Name)
					}
				}
			case err := <-watcher.Errors:
				{
					log.Println("error : ", err)
					return
				}
			}
		}
	}()

	err = watcher.Add("D:\\go\\go-cqhttp-0.9.34\\js\\a.js")
	if err != nil {
		log.Fatal(err)
	}
	<-done
}

//z := make(map[string]int)
func TestX(t *testing.T) {

	z脚本监控("D:\\go\\go-cqhttp-0.9.34\\js\\a.js")
}

func z脚本监控(path string) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()
	done := make(chan bool)
	go func() {
		for {
			select {
			case ev := <-watcher.Events:
				{
					log.Println(ev)
					//判断事件发生的类型，如下5种
					// Create 创建
					// Write 写入
					// Remove 删除
					// Rename 重命名
					// Chmod 修改权限
					ok := strings.HasSuffix(ev.Name, ".js")
					if ok {
						if ev.Op&fsnotify.Remove == fsnotify.Remove || ev.Op&fsnotify.Rename == fsnotify.Rename {
							delete(vms, ev.Name)
						}
						if ev.Op&fsnotify.Write == fsnotify.Write {
							z := ev.Name
							txt, err := ReadAll(z)
							if err != nil {
								println(err.Error())
							} else {
								vm := NewOttoVm()
								_, err = vm.Run(string(txt))
								if err != nil {
									println("脚本文件初始化错误 未加载 :\n" + z + "\n" + err.Error())
								} else {
									println("已重新加载脚本 " + z)
									delete(vms, z)
									vms[z] = vm
									//vms = append(vms,vm)

									for k, _ := range vms {
										log.Println("k-" + k)
									}
								}

							}
							//log.Println("写入文件 : ", ev.Name);
						}
					}

					if ev.Op&fsnotify.Create == fsnotify.Create {
						log.Println("创建文件 : ", ev.Name)
					}
					if ev.Op&fsnotify.Write == fsnotify.Write {
						log.Println("写入文件 : ", ev.Name)
					}
					if ev.Op&fsnotify.Remove == fsnotify.Remove {
						log.Println("删除文件 : ", ev.Name)
					}
					if ev.Op&fsnotify.Rename == fsnotify.Rename {
						log.Println("重命名文件 : ", ev.Name)
					}
					if ev.Op&fsnotify.Chmod == fsnotify.Chmod {
						log.Println("修改权限 : ", ev.Name)
					}
				}
			case err := <-watcher.Errors:
				{
					log.Println("error : ", err)
					return
				}
			}
		}
	}()

	err = watcher.Add(path)
	if err != nil {
		log.Fatal(err)
	}
	<-done
}

func TestAdminGetConfigJson(t *testing.T) {

	vm := NewOttoVm()
	//
	//	vm.Run(`写全局变量(123,123);
	//console.log(读全局变量(123)+1)
	//console.error(执行('for(var a=0;a<5;a++){console.log(a)}12')+"*-123")
	//console.error(执行('for(;;){}12')+"*-123")
	//`)

	//_,err :=	vm.Run(`
	//var rtMap = new 键值组();
	////给当前Map集合Put值
	//rtMap.添加("1", "hello");
	//rtMap.添加("2", "good");
	////获取当前Map的数组形式
	//var rtArray = [];
	////rtArray = rtMap.values();
	////通过值获取键
	//var keystr = rtMap.取键("hello");
	//console.log(keystr)
	////判断是否含有该值
	//if (rtMap.包含值("hello")) {
	//	console.log("含有该值");
	//}else{
	//	console.log("不含有该值");
	//}
	//console.log(1111)
	//if (rtMap.包含键("hello")) {
	//	console.log("1含有该键");
	//}else{
	//	console.log("1不含有该键");
	//}
	//`)
	//t.Log(err)
	_, err := vm.Run(`
function 时间格式化(fmt, date) {
    var ret;
    var opt = {
        "Y+": date.getFullYear().toString(),        // 年
        "m+": (date.getMonth() + 1).toString(),     // 月
        "d+": date.getDate().toString(),            // 日
        "H+": date.getHours().toString(),           // 时
        "M+": date.getMinutes().toString(),         // 分
        "S+": date.getSeconds().toString()          // 秒
        // 有其他格式化字符需求可以继续添加，必须转化成字符串
    };
    for (var k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k]))
        };
    };
    return fmt;
}


console.log(时间格式化("YYYY-mm-dd HH:MM:SS",new Date(1000)))
`)
	log.Println(err)
}

func TestName(t *testing.T) {
	vm := NewOttoVm()
	vm.Run(`
变量 一个数 = 2
如果 (一个数>1)
console.log("好大")
否则
console.log("好小")
`)
}

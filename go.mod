module wsjs

go 1.16

require (
	github.com/fsnotify/fsnotify v1.5.1
	github.com/gorilla/websocket v1.4.2
	github.com/robertkrimen/otto v0.0.0
	github.com/sirupsen/logrus v1.8.1
	gopkg.in/yaml.v2 v2.2.8 // indirect
)

replace github.com/robertkrimen/otto => ./otto

var zdate = new Date(Date.now());
var 日期 = zdate.getDate();
var 早安表 = new 键值组();
var 晚安表 = new 键值组();
var 流星 = "[CQ:smallemoji,id1=4194325,id2=2,txt=暖群机器人]"

function onevent(s) {
    //{"anonymous":null,"font":0,"group_id":1135595556,"message":"[CQ:face,id=179]","message_id":47066301,"message_type":"group","post_type":"message","raw_message":"[CQ:face,id=179]","self_id":2023291072,"sender":{"age":0,"area":"","card":"","level":"","nickname":"zj1d","role":"member","sex":"unknown","title":"","user_id":1264666489},"sub_type":"normal","time":1608639946,"user_id":1264666489}

    var 当前日期 = new Date(Date.now()).getDate()
    if (日期 != 当前日期) {
        早安表.清空()
        晚安表.清空()
        日期= 当前日期
    }
    var 回复 = ""
    if (s.post_type == "message") {
        if (s.message.包含("早") || s.message.包含("猫宁") || s.message.包含("猫宁") || s.message.toLowerCase().包含("morning")) {
            if (早安表.包含键(s.user_id)) {
                if (概率(30)) {
                    回复 = "# " + s.sender.nickname + " 不早了 你已经在" + 时间格式化("H点M分S秒", new Date(早安表.取值(s.user_id))) + "早起了o";
                    console.log(早安表.取值(s.user_id))
                }
            } else {
                早安表.添加(s.user_id, Date.now())
                if (早安表.大小() <21) 送礼物(s.group_id,s.user_id,随机数(13))
                回复 = "# " + s.sender.nickname + " 早睡早起 你是今天第" + 早安表.大小() + "个早起的人~"
            }
        } else if (s.message.包含("晚") || s.message.包含("奈特") || s.message.包含("古耐") || s.message.toLowerCase().包含("night")) {
            if (晚安表.包含键(s.user_id)) {
                if (概率(30))
                    回复 = "# " + s.sender.nickname + " 你好像已经在" + 时间格式化("H点M分S秒", new Date(晚安表.取值(s.user_id))) + "睡觉了o"
            } else if (!早安表.包含键(s.user_id)) {
                if (概率(30))
                    回复 = "# " + s.sender.nickname + " 快醒醒 你还没起 怎么就睡了o"
            } else {
                晚安表.添加(s.user_id, Date.now())
                var 时间差 = Date.now() - 早安表.取值(s.user_id)
                var 小时 = Math.floor(时间差 / (3600 * 1000))
                var 分钟 = Math.floor(时间差 % (3600 * 1000) / ((60 * 1000)))
                var 秒钟 = Math.floor(时间差 % (3600 * 1000) % ((60 * 1000)) / (1000))
                回复 = "# " + s.sender.nickname + " 祝你好梦 你是今天第" + 晚安表.大小() + "个早睡的人~" + "\r" + "今天共活跃了" + 小时 + "小时" + 分钟 + "分" + 秒钟 + "秒" + " 明天也要继续努力o"
            }
        }
        if (回复 == null || 回复.length == 0) return;
        if (s.message_type == "private")
            发消息(s.user_id, 流星 + 回复)
        else
            发群消息(s.group_id, 流星 + 回复)
    }

}
function 送礼物(群号,用户,id){
    发群消息(群号, "[CQ:gift,qq="+用户+",id="+id+"]")
}

function 时间格式化(fmt, date) {
    var ret;
    var opt = {
        "Y+": date.getFullYear().toString(),        // 年
        "m+": (date.getMonth() + 1).toString(),     // 月
        "d+": date.getDate().toString(),            // 日
        "H+": date.getHours().toString(),           // 时
        "M+": date.getMinutes().toString(),         // 分
        "S+": date.getSeconds().toString()          // 秒
        // 有其他格式化字符需求可以继续添加，必须转化成字符串
    };
    for (var k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k]))
        }
    }
    return fmt;
}
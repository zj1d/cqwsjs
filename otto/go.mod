module otto

go 1.15

require (
	github.com/robertkrimen/otto v0.0.0
	gopkg.in/sourcemap.v1 v1.0.5 // indirect
)

replace github.com/robertkrimen/otto => ../otto
